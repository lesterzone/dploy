package main

import (
	"bufio"
	"errors"
	"flag"
	"log"
	"os"
	"os/exec"
	"strconv"
	"time"
)

// Tech ...nology to be used to deploy
var Tech string

// Path for project's path
var Path string

func init() {
	flag.StringVar(&Tech, "tech", "", "Technology to be deployed")
	flag.StringVar(&Path, "path", "./", "Path for project")
}

func main() {
	flag.Parse()

	log.Println("Starting!")
	log.Println("Let's check if ruby is installed, with `ruby -v`")
	err := isRubyInstalled()
	if err != nil {
		log.Println("looks like ruby is not installed", err)
		return
	}

	log.Println("Let's check if capistrano is installed, with `cap -v`")
	err = isCapistranoInstalled()
	if err != nil {
		log.Println("looks like capistrano is not installed", err)
		answer := ask("Install capistrano? y/n\t")
		if answer == "n" {
			log.Println("Nothing to do here kido, have a good day")
			return
		}

		log.Println("install capistrano and continue")
	}

	err = isCapistranoSet()
	if err == nil {
		log.Println("Looks like there's a capistrano config")
		answer := ask("Override capistrano? y/n\t")
		if answer == "n\n" {
			log.Println("Nothing to do here kido, have a good day")
			return
		}
		backupConfig()
		overrideConfig()
	}

	log.Println("Seting up capistrano")

	rails := &Rails{}
	nodejs := &Nodejs{}

	switch Tech {
	case "rails":
		err = validate(rails)

	case "nodejs":
		err = validate(nodejs)
	default:
		err = errors.New("Technology not enabled...yet")
	}

	if err != nil {
		log.Println("Error:", err)
		return
	}

	log.Println("Looks like we are ready to start deploy")
}

func overrideConfig() error {

	return nil
}

func backupConfig() error {
	log.Println("Creating a copy of the existing configuration")
	files := []string{
		"Capfile", "config/deploy.rb", "config/deploy/staging.rb",
		"config/deploy/production.rb",
	}

	for _, file := range files {
		if !fileExist(file) {
			continue
		}

		cmd := "cp"
		name := file + "-" + strconv.FormatInt(time.Now().Unix(), 10) + ".backup"
		args := []string{file, name}

		out, err := exec.Command(cmd, args...).Output()
		log.Println(string(out))

		if err != nil {
			log.Println("error on copying file", file, err, out)
			return err
		}
	}

	return nil
}

func ask(question string) string {
	reader := bufio.NewReader(os.Stdin)
	log.Println(question)
	text, _ := reader.ReadString('\n')
	return string(text)
}

// By looking at files we can ensure proper Capistrano configuration
func isCapistranoSet() error {
	files := []string{
		"Capfile", "config/deploy.rb",
	}

	return checkFiles(files)
}

func isCapistranoInstalled() error {
	cmd := "cap"
	args := []string{"-v"}

	out, err := exec.Command(cmd, args...).Output()
	// todo check the version of ruby, we are looking for `Version: 3.5`
	log.Println(string(out))

	return err
}

func isRubyInstalled() error {
	cmd := "ruby"
	args := []string{"-v"}

	out, err := exec.Command(cmd, args...).Output()
	// todo check the version of ruby, we are looking for `ruby v2.`
	log.Println(string(out))

	return err
}

func validate(tech Technology) error {
	return checkFiles(tech.Dependencies())
}

// Technology to be deployed
type Technology interface {
	Dependencies() []string
}

// Object to inherit methods for different technologies
type Object struct{}

// Rails technology
type Rails struct {
	Object
	Title string
}

// Nodejs technology
type Nodejs struct {
	Object
}

// Dependencies for technology
func (object *Object) Dependencies() []string {
	return []string{}
}

// Dependencies for Rails technology
func (rails *Rails) Dependencies() []string {
	return []string{
		"Gemfile", "Gemfile.lock", "Procfile",
	}
}

// checkFiles based on []dependencies
func checkFiles(files []string) error {
	for _, item := range files {
		exist := fileExist("./" + item)

		if !exist {
			return errors.New("File not found: " + item)
		}
	}

	return nil
}

// Dependencies for Nodejs technology
func (nodejs Nodejs) Dependencies() []string {
	return []string{
		"package.json",
	}
}

func fileExist(filePath string) bool {
	if _, err := os.Stat(filePath); os.IsNotExist(err) {
		return false
	}

	return true
}
