package rails

var deploy = `
# config valid only for current version of Capistrano
lock '3.5.0'

set :application, {{.app_name}}

# set :branch, :{{.branch}}
set :repo_url, '{{.repo_url}}'

# Default value for :linked_files is []
set :linked_files, fetch(:linked_files, []).
  push({{.linked_files}})

# Default value for linked_dirs is []
set :linked_dirs, fetch(:linked_dirs, []).
  push({{.linked_dirs}})

namespace :deploy do
  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute :sudo, :service, "{{.restart}}"
    end
  end

  after :publishing, 'deploy:restart'
  after :finishing, 'deploy:cleanup'
end

`
